var express = require('express');
var router = express.Router();
var config = require('../config');
var request = require('request');

var auth_grant = {
    oauth_link: "https://bitbucket.org/account/user/bitbucket-cloud-dev/api",
    response_type: "code",
    type: {
        auth_grant_type : "Authorization Code Grant",
        definition: "This is the most common auth grant. " +
        "You see this everyday when you log in to Facebook or Google. " +
        "The difference of this auth grant is it is user initiated via the user-agent/browser." +
        "\nThe authorization code is obtained by using an authorization server as an intermediary between the client and resource owner.  Instead of requesting authorization directly from the resource owner, the client directs the resource owner to an authorization server (via its user- agent as defined in which in turn directs the resource owner back to the client with the authorization code. " +
        "\nBefore directing the resource owner back to the client with the authorization code, the authorization server authenticates the resource owner and obtains authorization.  Because the resource owner only authenticates with the authorization server, the resource owner's credentials are never shared with the client.",
        when_to_use: "Security Benefit",
        diff_with_other_grants: [
            "User initiated grant through user-agent/browser",
            "Resource Owner credentials are never shared because resource owner interacts with the Authorization server and not the client",
            "Provides security benefit by authenticating client and returning the access token to the client directly without passing to the resource owner's user-agent (browser)"
        ],
        document_link : "https://developer.atlassian.com/cloud/bitbucket/oauth-2/",
        rfc_link: "https://tools.ietf.org/html/draft-ietf-oauth-v2-31#section-1.3.1"
    },
    client: {
        key: config.consumerKey,
        secret: config.consumerSecret
    }
};

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', auth_grant);
});

router.get(`/healthcheck`, function(req, res) {
  res.send('OK');
});

/*
 * Step 1: This endpoint that authorizes the app for access to the end-user data
 */
router.post('/authorize', function(req, res) {
    console.log(`User's trying to authorize consumer_key: ${req.body.client_key_input} using Authorization Code Grant flow`);
    console.log('Save consumer key and secret to be used in /oauth-callback');
    process.env.client_key_input = req.body.client_key_input;
    process.env.client_secret_input = req.body.client_secret_input;

    res.redirect(`https://bitbucket.org/site/oauth2/authorize?client_id=${req.body.client_key_input}&response_type=${auth_grant.response_type}`);
});

/**
 * Step 2: This endpoint processes the temporary code sent by Bitbucket's Authorization Server
 * and exhanges that for an access_token, which can be used to invoke APIs
 *
 * The console logs here serve as a guidline and walks you through the process of the exchange and calling a REST API
 */
router.get('/oauth-callback', function(req, res) {
    let code = req.query.code;
    console.log(`Callback from authorization server with temporary code: ${code} to be exchange for an access_token`);
    console.log('==================================================================');
    console.log('Start building our request to exchange this with an access_token');
    console.log('==================================================================');
    console.log(`Generate basic authentication of consumer_key:consumer_secret: ${process.env.client_key_input}:${process.env.client_secret_input}`);
    let digested = new Buffer(`${process.env.client_key_input}:${process.env.client_secret_input}`).toString('base64');

    var access_token_options = { method: 'POST',
            url: 'https://bitbucket.org/site/oauth2/access_token',
            headers:
                {
                    'Cache-Control': 'no-cache',
                    Authorization: `Basic ${digested}`,
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
            form: { grant_type: 'authorization_code', code: `${code}` }
        };

    console.log(`Request: \n ${JSON.stringify(access_token_options, null, 2)}`);

    request(access_token_options, function (error, response, body) {
        if (error) throw new Error(error);
        let body_json = JSON.parse(body);
        let access_token = body_json.access_token;

        console.log(`Response: \n ${JSON.stringify(body_json, null, 2)}`);
        console.log(`Callback from authorization server with access_token: ${access_token} to be used for API requests`);

        console.log('==================================================================');
        console.log(`Start building our request to GET end-user's repository with access_token : ${access_token}`);
        console.log('==================================================================');
        let options = { method: 'GET',
            url: 'https://api.bitbucket.org/2.0/repositories',
            headers:
                {
                    'Cache-Control': 'no-cache',
                    Authorization: `Bearer ${access_token}` } };

        console.log(`Request\n ${JSON.stringify(options, null, 2)}`)

        request(options, function (error, response, body) {
            if (error) throw new Error(error);
            //console.log(`Response: /n ${JSON.stringify(body_json, null, 2)}`);

            res.render('request',{
                access_token: {
                    access_token : access_token,
                    request: JSON.stringify(access_token_options, null, 2),
                    response: JSON.stringify(body_json, null, 2)
                },

                repository: {
                    request: JSON.stringify(options, null, 2),
                    response: JSON.stringify(JSON.parse(body), null, 2)
                },
                type: {
                    auth_grant_type: auth_grant.type.auth_grant_type
                }
            });
        });
    });
});

module.exports = router;
